FROM node:18.20.4-alpine
WORKDIR /avispa
COPY nuxt.config.ts /avispa/
COPY package*.json /avispa/
COPY .output /avispa/.output
ENV NODE_ENV production
EXPOSE 3000
CMD ["npm", "run", "start"]
