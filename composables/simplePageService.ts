export interface SimplePageData {
  content: string,
}

export function getSimplePage(name: string) {
  const config = useRuntimeConfig();
  const url = `${config.public.backendUrl}/${name}`;
  return useFetch<SimplePageData>(url);
}
