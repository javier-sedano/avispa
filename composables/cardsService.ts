export interface CardData {
  title: string,
  content: string,
  enabled: boolean,
  gitlabUrl: string,
  url: string,
  image: {
    url: string,
    alternativeText: string,
  },
}

export function getCards() {
  const config = useRuntimeConfig();
  return useFetch<CardData[]>(config.public.backendUrl + '/cards?_sort=order:ASC');
}
