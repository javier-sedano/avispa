import { marked } from 'marked';
import sanitizeHtml from 'sanitize-html';

export function md2html(markdown?: string): string {
  if (markdown === undefined) {
    return '';
  }
  const config = useRuntimeConfig();
  const uploadsRegex = /\(\/uploads\//g;
  const markdownWithAbsoluteImgs = markdown.replace(
    uploadsRegex,
    '(' + config.public.backendUrl + '/uploads/',
  );
  const html = marked(markdownWithAbsoluteImgs, { async: false });
  console.log(html);
  const allowedTags = sanitizeHtml.defaults.allowedTags.concat(['img']);
  const allowedAttributes = sanitizeHtml.defaults.allowedAttributes;
  allowedAttributes.img.push('class');
  return sanitizeHtml(html, {
    allowedTags,
    allowedAttributes,
  });
}
