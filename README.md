Reference architecture for web page with CMS in Strapi (headless CMS) and frontend in Vue/NuxtJS.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/avispa/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/avispa/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.avispa&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.avispa)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.avispa)
</details>

Keywords: CMS, Headless CMS, Strapi, Vue, Nuxt, w3css, vscode, devcontainer

Local strapidb: admin@admin.com / adm1N.com  (do not use it in production!!)

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* NPM SCRIPTS will show the available tasks:
  * Run "initLfs" to pull Git LFS files (only needed the first time just after clone)
  * Run "dependenciesInit" to download dependencies
  * Run "cms:resetDb" to copy reference CMS database from assets/baseDb/data.db to .strapi_tmp/ (this can be done whenever a new database is available)
  * Run "start:dev" to start the CMS and frontend at the same time
    * Browse http://localhost:4009/avispa/ for the frontend
    * Browse http://localhost:4010/admin/ for the CMS (or use the link in the frontend)
      * Administrator: admin@admin.com / adm1N.com  (do not use it in production!!)
  * Most common development tasks are configured as npm tasks

TODO:
* Bring DB from prod

Useful links:

* https://strapi.io/documentation/developer-docs/latest/getting-started/introduction.html
* https://vuejs.org/
* https://nuxtjs.org/
