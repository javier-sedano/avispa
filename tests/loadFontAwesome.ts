import { library, config } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
export { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

export function loadFontAwesome() {
  config.autoAddCss = false;
  library.add(fas, fab);
}
