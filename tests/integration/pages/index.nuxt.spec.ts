import { describe, expect, test, vi } from 'vitest';
import { mountSuspended } from '@nuxt/test-utils/runtime';
import { registerEndpoint } from '@nuxt/test-utils/runtime'
import Index from '~/pages/index.vue';

const BACKEND_PREFIX = '/backend';
const TEST_MARKDOWN = 'Some **Markdown**'
  + ' <script>alert("hacked");</script>'
  + ' <clean>clean</clean>'
  + ' ![test.png](/uploads/test.png)';
const ENABLED_CARD_WITH_LINK: CardData = {
  title: 'lorem enabled w/link',
  content: TEST_MARKDOWN,
  enabled: true,
  gitlabUrl: 'https://gitlab.com/javier-sedano/loremEnabledWLink',
  url: '/lorem_enabled_w_link',
  image: {
    "url": "/uploads/lorem_enabled_w_link.png",
    "alternativeText": "LoremEnabledWLink",
  },
};
const ENABLED_CARD_WITHOUT_LINK: CardData = {
  title: 'lorem enabled wo/link',
  content: TEST_MARKDOWN,
  enabled: true,
  gitlabUrl: '',
  url: '/lorem_enabled_wo_link',
  image: {
    "url": "/uploads/lorem_enabled_wo_link.png",
    "alternativeText": "LoremEnabledWoLink",
  },
};
const DISABLED_CARD_WITH_LINK: CardData = {
  title: 'lorem disabled w/link',
  content: TEST_MARKDOWN,
  enabled: false,
  gitlabUrl: 'https://gitlab.com/javier-sedano/loremEnabledWLink',
  url: '/lorem_disabled_w_link',
  image: {
    "url": "/uploads/lorem_disabled_w_link.png",
    "alternativeText": "LoremDisabledWLink",
  },
};
const DISABLED_CARD_WITHOUT_LINK: CardData = {
  title: 'lorem disabled wo/link',
  content: TEST_MARKDOWN,
  enabled: false,
  gitlabUrl: '',
  url: '/lorem_disabled_wo_link',
  image: {
    "url": "/uploads/lorem_disabled_wo_link.png",
    "alternativeText": "LoremDisabledWoLink",
  },
};

const cardsEndpointMock = vi.fn();
registerEndpoint('/backend/cards', cardsEndpointMock);

async function testWithCardData(cardData: CardData) {
  cardsEndpointMock.mockImplementationOnce(() => ([
    cardData,
  ]));
  const wrapper = await mountSuspended(Index);
  expect(wrapper.element).toMatchSnapshot('Initial');
  const card = wrapper.find('[data-test-id~="card"]');
  expect(card.find('[data-test-id~="header"]').text()).toEqual(cardData.title);
  const urls = card.findAll('[data-test-id~="url"]');
  urls.forEach((url) => {
    expect(url.attributes().href).toEqual(cardData.url);
  });
  const image = card.find('[data-test-id~="image"]');
  expect(image.attributes().src).toEqual(BACKEND_PREFIX + cardData.image.url);
  if (cardData.gitlabUrl) {
    expect(card.find('[data-test-id~="gitlabUrl"]').attributes().href).toEqual(cardData.gitlabUrl);
  } else {
    expect(card.find('[data-test-id="gitlabUrl"]').exists()).toBe(false);
  }
  expect(card.find('[data-test-id~="content"]').element).toMatchSnapshot('content');
  if (cardData.enabled) {
    expect(card.find('[data-test-id~="goButton"]').text()).toEqual('Go');
  } else {
    expect(card.find('[data-test-id~="goButton"]').text()).toEqual('(Currently disabled)');
  }
}


describe('Home page', () => {

  test('It renders enabled card with link', async () => {
    await testWithCardData(ENABLED_CARD_WITH_LINK);
  });

  test('It renders enabled card without link', async () => {
    await testWithCardData(ENABLED_CARD_WITHOUT_LINK);
  });

  test('It renders disabled card with link', async () => {
    await testWithCardData(DISABLED_CARD_WITH_LINK);
  });

  test('It renders disabled card without link', async () => {
    await testWithCardData(DISABLED_CARD_WITHOUT_LINK);
  });

  test('It renders several cards', async () => {
    cardsEndpointMock.mockImplementationOnce(() => ([
      ENABLED_CARD_WITH_LINK,
      ENABLED_CARD_WITHOUT_LINK,
      DISABLED_CARD_WITH_LINK,
      DISABLED_CARD_WITHOUT_LINK,
    ]));
    const wrapper = await mountSuspended(Index);
    expect(wrapper.element).toMatchSnapshot('Initial');
    const cards = wrapper.findAll('[data-test-id~="card"]');
    expect(cards.length).toEqual(4);
  });


  test('It renders zero cards', async () => {
    cardsEndpointMock.mockImplementationOnce(() => ([
    ]));
    const wrapper = await mountSuspended(Index);
    expect(wrapper.element).toMatchSnapshot('Initial');
    const cards = wrapper.findAll('[data-test-id~="card"]');
    expect(cards.length).toEqual(0);
  });

});
