import { describe, expect, test } from 'vitest';
import { mountSuspended } from '@nuxt/test-utils/runtime';
import Contact from '~/pages/contact.vue';

describe('Contact page', () => {

  test('It renders the initial html', async () => {
    const wrapper = await mountSuspended(Contact);
    expect(wrapper.element).toMatchSnapshot('Initial');
  });

});
