import { describe, expect, test } from 'vitest';
import { mountSuspended } from '@nuxt/test-utils/runtime';
import Description from '~/pages/description.vue';
import { registerEndpoint } from '@nuxt/test-utils/runtime'

registerEndpoint('/backend/description', () => ({
  content: 'Some **Markdown**'
    + ' <script>alert("hacked");</script>'
    + ' <clean>clean</clean>'
    + ' ![test.png](/uploads/test.png)',
}));

describe('Description page', () => {

  test('It renders the initial html', async () => {
    const wrapper = await mountSuspended(Description);
    expect(wrapper.element).toMatchSnapshot('Initial');
  });

});
