import { nextTick } from 'vue';
import { describe, expect, test } from 'vitest';
import { mountSuspended } from '@nuxt/test-utils/runtime';
import Default from '~/layouts/default.vue';
import { loadFontAwesome, FontAwesomeIcon } from '~/tests/loadFontAwesome';

loadFontAwesome();

describe('Defaut layout', () => {

  test('It renders the initial html', async () => {
    const wrapper = await mountSuspended(Default, {
      global: {
        stubs: {
          FontAwesomeIcon,
        },
      },
    });
    expect(wrapper.element).toMatchSnapshot('Initial');
    wrapper.findAll('[data-test-id~="adminLink"]').forEach((adminLink) => {
      console.log(adminLink.attributes().href);
      expect(adminLink.attributes().href).toEqual('/backend/admin/');
    });
    expect(wrapper.find('[data-test-id~="version"]').text()).toEqual('Version test');

  });

  test('It toggles the menu', async () => {
    const wrapper = await mountSuspended(Default, {
      global: {
        stubs: {
          FontAwesomeIcon,
        },
      },
      attachTo: document.body,
    });
    expect(wrapper.element).toMatchSnapshot('Initial without menu');
    expect(wrapper.find('[data-test-id~="menu"]').isVisible()).toEqual(false);
//    (wrapper.vm as any).toggleMenu(); // This also works
    wrapper.find('[data-test-id~="menuShow"]').trigger('click');
    await nextTick();
    expect(wrapper.find('[data-test-id~="menu"]').isVisible()).toEqual(true);
    wrapper.find('[data-test-id~="menuHide"]').trigger('click');
    await nextTick();
    expect(wrapper.find('[data-test-id~="menu"]').isVisible()).toEqual(false);
  });

});
