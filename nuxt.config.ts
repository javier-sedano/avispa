const appVersion =
  require('./package.json').version +
  (process.env.NODE_ENV === 'production' ? '' : '-' + process.env.NODE_ENV);
const backendUrl = process.env.BACKEND_URL || 'http://localhost:4010';

export default defineNuxtConfig({
  compatibilityDate: '2024-04-03',
  app: {
    baseURL: '/avispa/',
    head: {
      title: 'Javier Sedano',
      htmlAttrs: {
        lang: 'en',
      },
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: '' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
      link: [
        { rel: 'icon', type: 'image/jpg', href: '/avispa/favicon.jpg' },
      ],
    },
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "~/assets/scss/variables.scss" as *;',
          api: 'modern-compiler',
        },
      },
    },
  },
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],
  runtimeConfig: {
    public: {
      appVersion,
      backendUrl,
    },
  },
  build: {
    transpile: ['@fortawesome/vue-fontawesome'],
  },
  modules: [
    '@nuxt/test-utils/module',
  ],
  devtools: { enabled: true },
  $test: {
    runtimeConfig: {
      public: {
        appVersion: 'test',
        backendUrl: '/backend',
      },
    },
  },
  experimental: {
    appManifest: false,
  },
})
