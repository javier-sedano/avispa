#!/bin/bash -e
pushd `dirname $0` > /dev/null

docker-compose -p avispa_devcontainer stop sonarqube

popd > /dev/null
