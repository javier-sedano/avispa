#!/bin/bash
pushd `dirname $0` > /dev/null

status=$(curl -s -w "%{http_code}\n" http://sonarqube:9000/api/server/version -o /dev/null)
if [ "$status" == "200" ]
then
  echo "Sonarqube available, reusing"
else
  docker-compose -p avispa_devcontainer up -d --build sonarqube

  echo -n "Waiting for sonarqube to be ready "
  while true
  do
    status=$(curl -s -w "%{http_code}\n" http://sonarqube:9000/api/server/version -o /dev/null)
    if [ "$status" == "200" ]
    then
      break
    fi
    echo -n "."
    sleep 1
  done
  echo ""
fi

echo -n "Testing if the admin password is set..."
status=$(curl -s -u admin:admin1 -w "%{http_code}\n" "http://sonarqube:9000/api/users/groups?login=admin" -o /dev/null)
if [ "$status" == 200 ]
then
  echo -n "ALREADY SET, SKIPPING"
else
  echo -n "not set, setting to admin/admin1..."
  sleep 2
  status=$(curl -s -u admin:admin -X POST -w "%{http_code}\n" "http://sonarqube:9000/api/users/change_password?login=admin&previousPassword=admin&password=admin1" -o /dev/null)
  if [ "$status" == "204" ]
  then
    echo -n "CHANGED"
  else
    echo "FAILED"
    exit 1
  fi
fi
echo ""

./sonarqube_token_export.sh

popd > /dev/null
