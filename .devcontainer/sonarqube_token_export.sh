#!/bin/bash
pushd `dirname $0` > /dev/null

TOKEN=$(./sonarqube_token.sh)

echo "export SONAR_TOKEN=${TOKEN}" > ../.sonar_token.sh

popd > /dev/null
