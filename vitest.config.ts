import { defineVitestConfig, } from '@nuxt/test-utils/config'
import { coverageConfigDefaults } from 'vitest/config'

export default defineVitestConfig({
  test: {
    coverage: {
      reporter: [
        'text',
        'lcov',
      ],
      exclude: [
        'cms',
        'nuxt.config.ts',
        ...coverageConfigDefaults.exclude,
      ],
      thresholds: {
        100: true,
      },
    },
  },
});
