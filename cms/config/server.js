module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: env('PUBLIC_URL', 'http://localhost:4010'),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'c97fb5f05794c2e177d8e8b56148b90a'),
    },
  },
});
