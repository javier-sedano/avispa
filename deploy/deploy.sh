#!/bin/bash
pushd `dirname "$0"`

TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
./deploy_cms.sh $TAG
./deploy_front.sh $TAG

