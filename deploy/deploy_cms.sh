#!/bin/bash
pushd `dirname "$0"`

TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/avispa/cms
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=avispa-cms" --filter "status=running" --quiet`
docker container stop avispa-cms
docker container rm avispa-cms
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create \
  -p 5780:1337 \
  -v /data/avispa-cms/.tmp:/avispa-cms/.tmp \
  -v /data/avispa-cms/uploads:/avispa-cms/public/uploads \
  -e STRAPI_TELEMETRY_DISABLED=true \
  -e PUBLIC_URL=https://jsedano.duckdns.org/avispa-cms \
  --restart unless-stopped \
  --name avispa-cms \
  $IMAGE
if [ "$containerId" ]
then
  docker container start avispa-cms
fi

popd
