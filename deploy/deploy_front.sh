#!/bin/bash
pushd `dirname "$0"`

TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/avispa/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=avispa" --filter "status=running" --quiet`
docker container stop avispa
docker container rm avispa
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create \
  -p 5781:3000 \
  -e NUXT_PUBLIC_BACKEND_URL=https://jsedano.duckdns.org/avispa-cms \
  --name avispa \
  $IMAGE
if [ "$containerId" ]
then
  docker container start avispa
fi

popd
